import TestDraw from "../../testdraw.jpg";

const SAVE_TYPE = ['image/png', 'image/jpeg', 'image/svg+xml'];
export const checkSaveType = (type) => SAVE_TYPE.includes(type);

export const DEFAULT_OPTIONS = {
  dotSize: (0.5 + 2.5) / 2,
  // minWidth: 0.5,
  minWidth: 0.5,
  // maxWidth: 2.5,
  maxWidth: 1.5,
  throttle: 5,
  minDistance: 5,
  backgroundColor: 'yellow',
  penColor: 'black',
  velocityFilterWeight: 0.7,
  onBegin: () => {},
  onEnd: () => {}
};

export const convert2NonReactive = (observerValue) =>
  JSON.parse(JSON.stringify(observerValue));

export const TRANSPARENT_PNG = {
  // src: TestDraw,
  src:
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=',
  x: 0,
  y: 0
};
